@echo off
cls

set DRIVE_LETTER=%1:
set FILE_NAME=%2
set NUM_PLAYERS=%3

set PATH=%DRIVE_LETTER%\Java\bin;%DRIVE_LETTER%\Java\ant-1.9.6\bin;c:\Windows

ant run -Ddrive-letter=%DRIVE_LETTER% -Dfile-name=%FILE_NAME% -Dnum-players=%NUM_PLAYERS%

