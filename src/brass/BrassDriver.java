package brass;

public class BrassDriver
{
	//DO THIS
	//process the save/load game file name parameter
	public static void main(String[] args)
	{
		int num_players;
		String brass_game_file_name;
	   
		brass_game_file_name = args[0];
		
		try
		{
			num_players = Integer.parseInt(args[1]);
		}
		catch (NumberFormatException nfe)
		{
			num_players = 4;
		}
	   
		BrassGame brass_game = new BrassGame(brass_game_file_name, num_players);
	}
}
